
    $(document).ready(function(){
      // Add an option where the user can change the background color(minimum of 3 colors) of the page(You can choose any color).
      var colors = ["color1", "color2", "color3"];
      $.each(colors, function(index, color){
        var option = $("<div class='color-option " + color + "'></div>");
        option.on("click", function(){
          $("body").css("background-color", $("." + color).css("background-color"));
        });
        $(".color-options").append(option);
      });

      // Show a preview of the image using a large modal when the image has been clicked.

      $("img").on("click", function(){
        var src = $(this).attr("src");
        var modal = $("<div class='preview-modal'></div>");
        var preview = $("<img src='" + src + "'>");
        modal.append(preview);
        $("body").append(modal);
        modal.fadeIn();
        preview.on("click", function(){
          modal.fadeOut();
        });
      });

      //Using jquery add a class named “lead”  to all <p> elements.
	  
	  
      $("p").addClass("lead");
    });
