
    $(document).ready(function(){
      // Make the content of the login form slidedown from the top after 2 seconds when the page loads.

      setTimeout(function(){
        $("#login-form").slideDown('slow');
      }, 2000);

      // Set the border color of the input elements to green when the user starts typing.
      $("input").on("focus", function(){
        $(this).css("border-color", "green");
      });

      // Using jquery add a class named “form-control-sm” to all input type elements.
      $("input").addClass("form-control-sm");

      // Disable and add a loader when the login button is clicked.

      $("#login-button").on("click", function(){
        $(this).attr("disabled", true);
        $(this).after("<img src='loader.gif' alt='Loading...' id='loader'>");
      });
    });
